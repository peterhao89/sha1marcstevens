/**************************************************************************\
|
|    Copyright (C) 2009 Marc Stevens
|
|    This program is free software: you can redistribute it and/or modify
|    it under the terms of the GNU General Public License as published by
|    the Free Software Foundation, either version 3 of the License, or
|    (at your option) any later version.
|
|    This program is distributed in the hope that it will be useful,
|    but WITHOUT ANY WARRANTY; without even the implied warranty of
|    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
|    GNU General Public License for more details.
|
|    You should have received a copy of the GNU General Public License
|    along with this program.  If not, see <http://www.gnu.org/licenses/>.
|
\**************************************************************************/

#include <stdexcept>
#include <iostream>
#include <fstream>
#include <vector>
#include <string>

#include <boost/lexical_cast.hpp>
#include <boost/program_options.hpp>

//#include <hashutil5/rng.hpp>
//#include <hashutil5/sdr.hpp>
//#include <hashutil5/timer.hpp>
//#include <hashutil5/booleanfunction.hpp>

#include "rng.hpp"
#include "sdr.hpp"
#include "timer.hpp"
#include "booleanfunction.hpp"

#include "Forwad_main.hpp"
#include "Backward_main.hpp"
#include "Connect_main.hpp"

using namespace hashutil;
using namespace std;
namespace po = boost::program_options;
namespace fs = boost::filesystem;

boost::mutex mut;
std::string workdir;
uint64 pbcount = 0;
int lastlowpathindex = -1;
vector< vector<unsigned> > badcounts(85, vector<unsigned>(32,0));


#if 0 //Backward_main.cpp

int main(int argc, char** argv) 
{
	hashutil::timer runtime(true);

	try {
		Backward_path_container_autobalance B_container;
		vector< vector<unsigned> > msgmask(16);
		unsigned msgoffset = 0;
		bool msglcext = false;
		string tunnelsfilename;
				
		// Define program options
		po::options_description 
			desc("Allowed options"), 
			msg("Define message differences (as bitnr, bitnr=0-31)"), 
			ihv("Define start IV (necessary for t=0 and t=1)"),
			all("Allowed options");

		desc.add_options()
			("help,h", "Show options.")
			("mod,m"
				, po::value<unsigned>(&B_container.modn)->default_value(1)
				, "Do only 1/m of all work.")

			("index,i"
				, po::value<unsigned>(&B_container.modi)->default_value(0)
				, "Do i-th part of all work (i=0,...,m-1).")

			("workdir,w"
				, po::value<string>(&workdir)->default_value("./data")
				, "Set working directory.")

			("inputfile,f"
				, po::value<string>(&B_container.inputfile)->default_value("")
				, "Use specified inputfile.")

			("newinputpath,n"
				, po::bool_switch(&B_container.newinputpath)
				, "Start with empty path.")

			("showinputpaths,s"
				, po::bool_switch(&B_container.showinputpaths)
				, "Show all input paths.\n")

			("tstep,t"
				, po::value<unsigned>(&B_container.t)
				, "Do step t (=0,...,15).")
				
			("trange"
				, po::value<unsigned>(&B_container.trange)->default_value(0)
				, "Number of additional steps to perform.")

			("maxconditions,c"
				, po::value<unsigned>(&B_container.maxcond)->default_value(2176)
				, "Limit total amount of bitconditions.")

			("condtend,b"
				, po::value<int>(&B_container.tend)->default_value(80)
				, "Set starting Q_t for maxconditions.")

			("maxQ26upcond"
				, po::value<unsigned>(&B_container.maxQ26upcond)->default_value(0)
				, "Limit conditions on Q_26 and up")

			("includenaf"
				, po::bool_switch(&B_container.includenaf)
				, "Include naf(Q_t+1) in # conditions.")

			("halfnafweight"
				, po::bool_switch(&B_container.halfnafweight)
				, "naf(Q_t+1) only weighs half in # cond.")

			("maxweight,p"
				, po::value<unsigned>(&B_container.maxweight)->default_value(32)
				, "Limit carry propagation.")
			("minweight"
				, po::value<unsigned>(&B_container.minweight)->default_value(0)
				, "Limit carry propagation.")

			("maxsdrs,q"
				, po::value<unsigned>(&B_container.maxsdrs)->default_value(2000)
				, "Limit total number of sdrs.")

			("autobalance,a"
				, po::value<unsigned>(&B_container.ubound)->default_value(0)
				, "Do autobalancing using target amount.")

			("fillfraction"
				, po::value<double>(&B_container.fillfraction)->default_value(0)
				, "Fill up to fraction of target amount\n\twith heavier paths than maxcond.")

			("estimate,e"
				, po::value<double>(&B_container.estimatefactor)->default_value(0)
				, "Do an estimate for maxcond.")

			("nafestimate"
				, po::value<unsigned>(&B_container.nafestweight)->default_value(8)
				, "Estimate of naf(Q_t+1) in # conditions.")

			("onemessagediff"
				, po::bool_switch(&B_container.onemessagediff)
				, "Use only one messagediff dmt")
			("expandprevmessagediff"
				, po::bool_switch(&B_container.expandprevmessagediff)
				, "Expand previous messagediff")
			("tunnelconditionsfile"
				, po::value<string>(&tunnelsfilename)->default_value("")
				, "Force compatibility with tunnels")
			;

		msg.add_options()	
			("diffm0", po::value< vector<unsigned> >(&msgmask[0]), "mask m0")
			("diffm1", po::value< vector<unsigned> >(&msgmask[1]), "mask m1")
			("diffm2", po::value< vector<unsigned> >(&msgmask[2]), "mask m2")
			("diffm3", po::value< vector<unsigned> >(&msgmask[3]), "mask m3")
			("diffm4", po::value< vector<unsigned> >(&msgmask[4]), "mask m4")
			("diffm5", po::value< vector<unsigned> >(&msgmask[5]), "mask m5")
			("diffm6", po::value< vector<unsigned> >(&msgmask[6]), "mask m6")
			("diffm7", po::value< vector<unsigned> >(&msgmask[7]), "mask m7")
			("diffm8", po::value< vector<unsigned> >(&msgmask[8]), "mask m8")
			("diffm9", po::value< vector<unsigned> >(&msgmask[9]), "mask m9")
			("diffm10", po::value< vector<unsigned> >(&msgmask[10]), "mask m10")
			("diffm11", po::value< vector<unsigned> >(&msgmask[11]), "mask m11")
			("diffm12", po::value< vector<unsigned> >(&msgmask[12]), "mask m12")
			("diffm13", po::value< vector<unsigned> >(&msgmask[13]), "mask m13")
			("diffm14", po::value< vector<unsigned> >(&msgmask[14]), "mask m14")
			("diffm15", po::value< vector<unsigned> >(&msgmask[15]), "mask m15")
			("diffoffset", po::value< unsigned >(&msgoffset), "mask offset")
			("difflcext", po::bool_switch(&msglcext), "extend to local coll.")
			;
		all.add(desc).add(msg);
	
		// Parse program options
		po::variables_map vm;
		po::store(po::parse_command_line(argc, argv, all), vm);
		{
			std::ifstream ifs("sha1diffpathbackward.cfg");
			if (ifs) po::store(po::parse_config_file(ifs, all), vm);
		}
		po::notify(vm);

		// Process program options
		if (vm.count("help") || vm.count("tstep")==0) {
			cout << desc << endl;
			return 0;
		}
		if (B_container.modi >= B_container.modn) {
			cerr << "Error: i must be strictly less than m!" << endl;
			return 1;
		}
		if (B_container.modn != 1) {
			cout << "Using set " << B_container.modi << " of " << B_container.modn << endl;
			B_container.trange = 0;
		}

                if (tunnelsfilename.size()) {
                        cout << "Loading tunnel conditions: " << tunnelsfilename << "..." << flush;
                        bool loadedtunnels = false;
                        try {
                                vector<sha1differentialpath> tmptunnel;
                                load_bz2(tmptunnel, text_archive, tunnelsfilename);
                                B_container.tunnelconditions = tmptunnel[0];
                                loadedtunnels = true;
                        } catch (exception &e) { cout << "error: " << endl << e.what() << endl; } catch (...) { cout << "unknown error!" << endl;}
                        if (loadedtunnels) {
                                cout << "done." << endl;
                                show_path(B_container.tunnelconditions);
                        }
                }

		uint32 me[16];
		for (unsigned k = 0; k < 16; ++k) {			
			me[k] = 0;
			for (unsigned j = 0; j < msgmask[k].size(); ++j)
				me[k] |= 1<<msgmask[k][j];
		}
		sha1_me_generalised(B_container.m_mask, me, msgoffset);
		if (msglcext) {
			uint32 met[16];
			uint32 mex[80];
			for (unsigned i = 0; i < 16; ++i)
				met[i] = rotate_left(me[i],5);
			sha1_me_generalised(mex, met, msgoffset+1);
			for (unsigned i = 0; i < 80; ++i)
				B_container.m_mask[i] ^= mex[i];

			sha1_me_generalised(mex, me, msgoffset+2);
			for (unsigned i = 0; i < 80; ++i)
				B_container.m_mask[i] ^= mex[i];

			for (unsigned i = 0; i < 16; ++i)
				met[i] = rotate_left(me[i],30);
			for (unsigned j = 3; j < 6; ++j) {
				sha1_me_generalised(mex, met, msgoffset+j);
				for (unsigned i = 0; i < 80; ++i)
					B_container.m_mask[i] ^= mex[i];
			}			
		}		

		// do not estimate if not autobalancing
		if (B_container.ubound == 0)
			B_container.estimatefactor = 0;

		// only estimate for larger ubound, not lower
		if (B_container.estimatefactor < 1)
			B_container.estimatefactor = 0;

		if (B_container.fillfraction < 0)
			B_container.fillfraction = 0;
		if (B_container.fillfraction > 1)
			B_container.fillfraction = 1;

		// Start job with given parameters
		B_container.set_parameters();
		for (unsigned tt = B_container.t; tt > B_container.t-B_container.trange; --tt) {
			cout << "delta_m[" << tt << "] = " << sdr(0,B_container.m_mask[tt]) << endl;
			Backward_path_container_autobalance containertmp = B_container;
			containertmp.t = tt;
			Backward_dostep(containertmp, true);
		}
		B_container.t -= B_container.trange;
		cout << "delta_m[" << B_container.t << "] = " << sdr(0,B_container.m_mask[B_container.t]) << endl;
		Backward_dostep(B_container);
	} catch (exception& e) {
		cout << "Runtime: " << runtime.time() << endl;
		cerr << "Caught exception!!:" << endl << e.what() << endl;
		throw;
	} catch (...) {
		cout << "Runtime: " << runtime.time() << endl;
		cerr << "Unknown exception caught!!" << endl;
		throw;
	}
	cout << "Runtime: " << runtime.time() << endl;
	return 0;
}

#endif //Backward_main.cpp


#if 0 //Forward_main.cpp
int main(int argc, char** argv) 
{
	hashutil::timer runtime(true);

	try {
		Forward_path_container_autobalance F_container;
		
		vector< vector<unsigned> > msgmask(16);
		unsigned msgoffset = 0;
		bool msglcext = false;
		string tunnelsfilename;
				
		// Define program options
		po::options_description 
			desc("Allowed options"), 
			msg("Define message differences (as bitnr, bitnr=0-31)"), 
			ihv("Define start IV (necessary for t=0 and t=1)"),
			all("Allowed options");

		desc.add_options()
			("help,h", "Show options.")
			("mod,m"
				, po::value<unsigned>(&F_container.modn)->default_value(1)
				, "Do only 1/m of all work.")

			("index,i"
				, po::value<unsigned>(&F_container.modi)->default_value(0)
				, "Do i-th part of all work (i=0,...,m-1).")

			("workdir,w"
				, po::value<string>(&workdir)->default_value("./data")
				, "Set working directory.")

			("inputfile,f"
				, po::value<string>(&F_container.inputfile)->default_value("")
				, "Use specified inputfile.")

			("newinputpath,n"
				, po::bool_switch(&F_container.newinputpath)
				, "Start with empty path.")

			("showinputpaths,s"
				, po::bool_switch(&F_container.showinputpaths)
				, "Show all input paths.\n")

			("tstep,t"
				, po::value<unsigned>(&F_container.t)
				, "Do step t (=0,...,15).")
				
			("trange"
				, po::value<unsigned>(&F_container.trange)->default_value(0)
				, "Number of additional steps to perform.")

			("maxconditions,c"
				, po::value<unsigned>(&F_container.maxcond)->default_value(2176)
				, "Limit total amount of bitconditions.")

			("condtbegin,b"
				, po::value<int>(&F_container.tbegin)->default_value(-3)
				, "Set starting Q_t for maxconditions.")

			("includenaf"
				, po::bool_switch(&F_container.includenaf)
				, "Include naf(Q_t+1) in # conditions.")

			("halfnafweight"
				, po::bool_switch(&F_container.halfnafweight)
				, "naf(Q_t+1) only weighs half in # cond.")

			("maxweight,p"
				, po::value<unsigned>(&F_container.maxweight)->default_value(32)
				, "Limit carry propagation.")
			("minweight"
				, po::value<unsigned>(&F_container.minweight)->default_value(0)
				, "Limit carry propagation.")

			("maxsdrs,q"
				, po::value<unsigned>(&F_container.maxsdrs)->default_value(2000)
				, "Limit total number of sdrs.")

			("autobalance,a"
				, po::value<unsigned>(&F_container.ubound)->default_value(0)
				, "Do autobalancing using target amount.")

			("fillfraction"
				, po::value<double>(&F_container.fillfraction)->default_value(0)
				, "Fill up to fraction of target amount\n\twith heavier paths than maxcond.")

			("estimate,e"
				, po::value<double>(&F_container.estimatefactor)->default_value(0)
				, "Do an estimate for maxcond.")

			("nafestimate"
				, po::value<unsigned>(&F_container.nafestweight)->default_value(8)
				, "Estimate of naf(Q_t+1) in # conditions.")

			("onemessagediff"
				, po::bool_switch(&F_container.onemessagediff)
				, "Use only one messagediff dmt")
			("expandprevmessagediff"
				, po::bool_switch(&F_container.expandprevmessagediff)
				, "Expand previous messagediff")
				
			("tunnelconditionsfile"
				, po::value<string>(&tunnelsfilename)->default_value("")
				, "Force compatibility with tunnels")
			;

		msg.add_options()	
			("diffm0", po::value< vector<unsigned> >(&msgmask[0]), "mask m0")
			("diffm1", po::value< vector<unsigned> >(&msgmask[1]), "mask m1")
			("diffm2", po::value< vector<unsigned> >(&msgmask[2]), "mask m2")
			("diffm3", po::value< vector<unsigned> >(&msgmask[3]), "mask m3")
			("diffm4", po::value< vector<unsigned> >(&msgmask[4]), "mask m4")
			("diffm5", po::value< vector<unsigned> >(&msgmask[5]), "mask m5")
			("diffm6", po::value< vector<unsigned> >(&msgmask[6]), "mask m6")
			("diffm7", po::value< vector<unsigned> >(&msgmask[7]), "mask m7")
			("diffm8", po::value< vector<unsigned> >(&msgmask[8]), "mask m8")
			("diffm9", po::value< vector<unsigned> >(&msgmask[9]), "mask m9")
			("diffm10", po::value< vector<unsigned> >(&msgmask[10]), "mask m10")
			("diffm11", po::value< vector<unsigned> >(&msgmask[11]), "mask m11")
			("diffm12", po::value< vector<unsigned> >(&msgmask[12]), "mask m12")
			("diffm13", po::value< vector<unsigned> >(&msgmask[13]), "mask m13")
			("diffm14", po::value< vector<unsigned> >(&msgmask[14]), "mask m14")
			("diffm15", po::value< vector<unsigned> >(&msgmask[15]), "mask m15")
			("diffoffset", po::value< unsigned >(&msgoffset), "mask offset")
			("difflcext", po::bool_switch(&msglcext), "extend to local coll.")
			;
		all.add(desc).add(msg);
		
		// Parse program options
		po::variables_map vm;
		po::store(po::parse_command_line(argc, argv, all), vm);
		{
			std::ifstream ifs("sha1diffpathforward.cfg");
			if (ifs) po::store(po::parse_config_file(ifs, all), vm);
		}
		po::notify(vm);

		// Process program options
		if (vm.count("help") || vm.count("tstep")==0) {
			cout << desc << endl;
			return 0;
		}
		if (F_container.modi >= F_container.modn) {
			cerr << "Error: i must be strictly less than m!" << endl;
			return 1;
		}
		if (F_container.modn != 1) {
			cout << "Using set " << F_container.modi << " of " << F_container.modn << endl;
			F_container.trange = 0;
		}


		if (tunnelsfilename.size()) {
			cout << "Loading tunnel conditions: " << tunnelsfilename << "..." << flush;
			bool loadedtunnels = false;
			try {
				vector<sha1differentialpath> tmptunnel;
				load_bz2(tmptunnel, text_archive, tunnelsfilename);
				F_container.tunnelconditions = tmptunnel[0];
				loadedtunnels = true;
			} catch (exception &e) { cout << "error: " << endl << e.what() << endl; } catch (...) { cout << "unknown error!" << endl;}
			if (loadedtunnels) {
				cout << "done." << endl;
				show_path(F_container.tunnelconditions);
			}
		}

		uint32 me[16];
		for (unsigned k = 0; k < 16; ++k) {			
			me[k] = 0;
			for (unsigned j = 0; j < msgmask[k].size(); ++j)
				me[k] |= 1<<msgmask[k][j];
		}
		sha1_me_generalised(F_container.m_mask, me, msgoffset);
		if (msglcext) {
			uint32 met[16];
			uint32 mex[80];
			for (unsigned i = 0; i < 16; ++i)
				met[i] = rotate_left(me[i],5);
			sha1_me_generalised(mex, met, msgoffset+1);
			for (unsigned i = 0; i < 80; ++i)
				F_container.m_mask[i] ^= mex[i];

			sha1_me_generalised(mex, me, msgoffset+2);
			for (unsigned i = 0; i < 80; ++i)
				F_container.m_mask[i] ^= mex[i];

			for (unsigned i = 0; i < 16; ++i)
				met[i] = rotate_left(me[i],30);
			for (unsigned j = 3; j < 6; ++j) {
				sha1_me_generalised(mex, met, msgoffset+j);
				for (unsigned i = 0; i < 80; ++i)
					F_container.m_mask[i] ^= mex[i];
			}			
		}		

		// do not estimate if not autobalancing
		if (F_container.ubound == 0)
			F_container.estimatefactor = 0;

		// only estimate for larger ubound, not lower
		if (F_container.estimatefactor < 1)
			F_container.estimatefactor = 0;

		if (F_container.fillfraction < 0)
			F_container.fillfraction = 0;
		if (F_container.fillfraction > 1)
			F_container.fillfraction = 1;

		// Start job with given parameters
		F_container.set_parameters();
		for (unsigned tt = F_container.t; tt < F_container.t+F_container.trange; ++tt) {
			cout << "delta_m[" << tt << "] = " << sdr(0,F_container.m_mask[tt]) << endl;
			Forward_path_container_autobalance containertmp = F_container;
			containertmp.t = tt;
			Forward_dostep(containertmp, true);
		}
		F_container.t += F_container.trange;
		cout << "delta_m[" << F_container.t << "] = " << sdr(0,F_container.m_mask[F_container.t]) << endl;

		Forward_dostep(F_container);
	} catch (exception& e) {
		cout << "Runtime: " << runtime.time() << endl;
		cerr << "Caught exception!!:" << endl << e.what() << endl;
		throw;
	} catch (...) {
		cout << "Runtime: " << runtime.time() << endl;
		cerr << "Unknown exception caught!!" << endl;
		throw;
	}
	cout << "Runtime: " << runtime.time() << endl;
	return 0;
}
#endif  //Forward_main.cpp


#if 1 //Connect_main.cpp
int main(int argc, char** argv) 
{
	hashutil::timer runtime(true);

	try {
		Connect_path_container C_container;
		vector< vector<unsigned> > msgmask(16);
		unsigned msgoffset = 0;
		bool msglcext = false;
		string tunnelsfilename;
				
		// Define program options
		po::options_description 
			desc("Allowed options"), 
			msg("Define message differences (as +bitnr and -bitnr, bitnr=1-32)"), 
			all("Allowed options");

		desc.add_options()
			("help,h", "Show options.")
			("mod,m"
				, po::value<unsigned>(&C_container.modn)->default_value(1)
				, "Do only 1/m of all work.")

			("index,i"
				, po::value<unsigned>(&C_container.modi)->default_value(0)
				, "Do i-th part of all work (i=0,...,m-1).")

			("workdir,w"
				, po::value<string>(&workdir)->default_value("./data")
				, "Set working directory.")

			("inputfilelow"
				, po::value<string>(&C_container.inputfilelow)
				, "Use specified inputfile for lower paths.")
                        ("loworder"
                                , po::value<unsigned>(&C_container.loworder)->default_value(0)
                                , "Order of processing low paths (0=random, 1=sorted, 2=w-sorted)")

			("inputfilehigh"
				, po::value<string>(&C_container.inputfilehigh)
				, "Use specified inputfile for upper paths.")
				
			("inputfileredo"
				, po::value<string>(&C_container.inputfileredo)
				, "Use specified inputfile to retest\nspecific lower and upper paths.")

			("showinputpaths,s"
				, po::bool_switch(&C_container.showinputpaths)
				, "Show all input paths.\n")

			("tstep,t"
				, po::value<unsigned>(&C_container.t)
				, "Do step t (=0,...,15).")

			("Qcondstart"
				, po::value<int>(&C_container.Qcondstart)->default_value(64)
				, "Count conditions from Q_t up.")
			("maxcond"
				, po::value<unsigned>(&C_container.bestpathcond)->default_value(262144)
				, "Maximum # conds")
			("keepall"
				, po::bool_switch(&C_container.keepall)
				, "Store all paths with cond <= maxcond")
			("determinelowestcond"
				, po::bool_switch(&C_container.determinelowestcond)
				, "Do not generate paths, only determine cond.")
			("splitmode"
				, po::value<unsigned>(&C_container.splitmode)
				, "Set splitmode: 0=upperpath, 2=lowerpath, +1=random")
                                
                        ("tunnelconditionsfile"
                                , po::value<string>(&tunnelsfilename)->default_value("")
                                , "Force compatibility with tunnels")
			("threads"
				, po::value<int>(&C_container.threads)->default_value(-1)
				, "Number of worker threads")
                        ("showstats"
                                , po::bool_switch(&C_container.showstats)
                                , "Show internal statistics for every thread every hour")
			;

		msg.add_options()	
			("diffm0", po::value< vector<unsigned> >(&msgmask[0]), "mask m0")
			("diffm1", po::value< vector<unsigned> >(&msgmask[1]), "mask m1")
			("diffm2", po::value< vector<unsigned> >(&msgmask[2]), "mask m2")
			("diffm3", po::value< vector<unsigned> >(&msgmask[3]), "mask m3")
			("diffm4", po::value< vector<unsigned> >(&msgmask[4]), "mask m4")
			("diffm5", po::value< vector<unsigned> >(&msgmask[5]), "mask m5")
			("diffm6", po::value< vector<unsigned> >(&msgmask[6]), "mask m6")
			("diffm7", po::value< vector<unsigned> >(&msgmask[7]), "mask m7")
			("diffm8", po::value< vector<unsigned> >(&msgmask[8]), "mask m8")
			("diffm9", po::value< vector<unsigned> >(&msgmask[9]), "mask m9")
			("diffm10", po::value< vector<unsigned> >(&msgmask[10]), "mask m10")
			("diffm11", po::value< vector<unsigned> >(&msgmask[11]), "mask m11")
			("diffm12", po::value< vector<unsigned> >(&msgmask[12]), "mask m12")
			("diffm13", po::value< vector<unsigned> >(&msgmask[13]), "mask m13")
			("diffm14", po::value< vector<unsigned> >(&msgmask[14]), "mask m14")
			("diffm15", po::value< vector<unsigned> >(&msgmask[15]), "mask m15")
			("diffoffset", po::value< unsigned >(&msgoffset), "mask offset")
			("difflcext", po::bool_switch(&msglcext), "extend to local coll.")
			;
		all.add(desc).add(msg);
	
		// Parse program options
		po::variables_map vm;
		po::store(po::parse_command_line(argc, argv, all), vm);
		{
			std::ifstream ifs("sha1diffpathconnect.cfg");
			if (ifs) po::store(po::parse_config_file(ifs, all), vm);
		}
		po::notify(vm);

		// Process program options
		if (vm.count("help") || vm.count("tstep")==0) {
			cout << desc << endl;
			return 0;
		}
		if (C_container.inputfilelow.size() == 0)
		{
			cerr << "Error: inputfilelow must be given!" << endl;
			return 2;
		}
		if (C_container.inputfilehigh.size() == 0)
		{
			cerr << "Error: inputfilehigh must be given!" << endl;
			return 2;
		}
		if (C_container.modi >= C_container.modn) {
			cerr << "Error: i must be strictly less than m!" << endl;
			return 1;
		}
		cout << "Total bitconditions bound: " << C_container.bestpathcond << endl;
		if (C_container.modn != 1)
			cout << "Using set " << C_container.modi << " of " << C_container.modn << endl;

		if (C_container.threads <= 0 || C_container.threads > boost::thread::hardware_concurrency())
			C_container.threads = boost::thread::hardware_concurrency();
                cout << "Using " << C_container.threads << " threads." << endl;
                
		uint32 me[16];
		for (unsigned k = 0; k < 16; ++k) {			
			me[k] = 0;
			for (unsigned j = 0; j < msgmask[k].size(); ++j)
				me[k] |= 1<<msgmask[k][j];
		}
		sha1_me_generalised(C_container.m_mask, me, msgoffset);
		if (msglcext) {
			uint32 met[16];
			uint32 mex[80];
			for (unsigned i = 0; i < 16; ++i)
				met[i] = rotate_left(me[i],5);
			sha1_me_generalised(mex, met, msgoffset+1);
			for (unsigned i = 0; i < 80; ++i)
				C_container.m_mask[i] ^= mex[i];

			sha1_me_generalised(mex, me, msgoffset+2);
			for (unsigned i = 0; i < 80; ++i)
				C_container.m_mask[i] ^= mex[i];

			for (unsigned i = 0; i < 16; ++i)
				met[i] = rotate_left(me[i],30);
			for (unsigned j = 3; j < 6; ++j) {
				sha1_me_generalised(mex, met, msgoffset+j);
				for (unsigned i = 0; i < 80; ++i)
					C_container.m_mask[i] ^= mex[i];
			}			
		}		

                if (tunnelsfilename.size()) {
                        cout << "Loading tunnel conditions: " << tunnelsfilename << "..." << flush;
                        bool loadedtunnels = false;
                        try {
                                vector<sha1differentialpath> tmptunnel;
                                load_bz2(tmptunnel, text_archive, tunnelsfilename);
                                C_container.tunnelconditions = tmptunnel[0];
                                loadedtunnels = true;
                        } catch (exception &e) { cout << "error: " << endl << e.what() << endl; } catch (...) { cout << "unknown error!" << endl;}
                        if (loadedtunnels) {
                                cout << "done." << endl;
                                show_path(C_container.tunnelconditions);
                        }
                }


		// Start job with given parameters
		Connect_dostep(C_container);

	} catch (exception& e) {
		cout << "Runtime: " << runtime.time() << endl;
		cerr << "Caught exception!!:" << endl << e.what() << endl;
		throw;
	} catch (...) {
		cout << "Runtime: " << runtime.time() << endl;
		cerr << "Unknown exception caught!!" << endl;
		throw;
	}
	cout << "Runtime: " << runtime.time() << endl;
	return 0;
}


#endif //Connect_main.cpp




