#ifndef QTFACE_H
#define QTFACE_H

#include <QtWidgets/QMainWindow>
#include "ui_qtface.h"
#include<qpushbutton.h>
#include <stdexcept>
#include <iostream>
#include <fstream>
#include <vector>
#include <string>

#include <boost/lexical_cast.hpp>
#include <boost/program_options.hpp>

#include <hashutil5/rng.hpp>
#include <hashutil5/sdr.hpp>
#include <hashutil5/timer.hpp>
#include <hashutil5/booleanfunction.hpp>

#include "Backward_main.hpp"
#include "Forwad_main.hpp"
#include "Connect_main.hpp"
#define bit32(x,n) (((x)>>(n))&1)
extern boost::mutex mut;
extern std::string workdir;
extern uint64 pbcount;
extern int lastlowpathindex;
extern vector< vector<unsigned> > badcounts;
extern hashutil::timer runtime;
extern Connect_path_container C_container;
extern Backward_path_container_autobalance B_container;
extern Forward_path_container_autobalance F_container;
extern std::string tunnelsfilename;

extern vector<sha1differentialpath> Forward_pathscache;
extern vector<sha1differentialpath> Backward_pathscache;

extern sha1differentialpath ForwardInitPath;
extern sha1differentialpath BackwardInitPath;

using namespace boost;

class QtFace : public QMainWindow
{
	Q_OBJECT

public:
	QtFace(QWidget *parent = 0);
	~QtFace();
	private slots:
	//void Myshow();

private:
	Ui::QtFaceClass ui;
private slots:    //�����źź���  
    void MyShowTest();
	void MdiffExpand_I();
	void ConfigForward();
	void SetBitCondition();
	void ExpandForward();

	void ConfigBackward();
	void SetBitConditionBack();
	void ExpandBackward();

	void ConfigConnect();
};

#endif // QTFACE_H
