#include "qtface.h"
#include <QFile>





QtFace::QtFace(QWidget *parent)
	: QMainWindow(parent)
{
	ui.setupUi(this);
	
}

void QtFace::MyShowTest()
{
	vector< vector<unsigned> > msgmask(16);

	QString i_qstring=ui.lineEdit->text();
	QString b_qstring=ui.lineEdit_2->text();
	unsigned msgoffset = i_qstring.toInt();
	unsigned diff_bit=b_qstring.toInt();
	msgmask[1].push_back((diff_bit+31)%32);
	msgmask[3].push_back((diff_bit+31)%32);
	msgmask[15].push_back(diff_bit%32);
	bool msglcext = true;
	uint32 me[16];
	for (unsigned k = 0; k < 16; ++k) {			
		me[k] = 0;
		for (unsigned j = 0; j < msgmask[k].size(); ++j)
			me[k] |= 1<<msgmask[k][j];
	}
	sha1_me_generalised(F_container.m_mask, me, msgoffset);
	sha1_me_generalised(B_container.m_mask, me, msgoffset);
	sha1_me_generalised(C_container.m_mask, me, msgoffset);
	if (msglcext) {
		uint32 met[16];
		uint32 mex[80];
		for (unsigned i = 0; i < 16; ++i)
			met[i] = rotate_left(me[i],5);

		sha1_me_generalised(mex, met, msgoffset+1);

		for (unsigned i = 0; i < 80; ++i)
		{
			F_container.m_mask[i] ^= mex[i];
			B_container.m_mask[i] ^= mex[i];
			C_container.m_mask[i] ^= mex[i];
		}

		sha1_me_generalised(mex, me, msgoffset+2);

		for (unsigned i = 0; i < 80; ++i)
		{
			F_container.m_mask[i] ^= mex[i];
			B_container.m_mask[i] ^= mex[i];
			C_container.m_mask[i] ^= mex[i];
		}

		for (unsigned i = 0; i < 16; ++i)
			met[i] = rotate_left(me[i],30);

		for (unsigned j = 3; j < 6; ++j) {
			sha1_me_generalised(mex, met, msgoffset+j);

			for (unsigned i = 0; i < 80; ++i)
			{
				F_container.m_mask[i] ^= mex[i];
				B_container.m_mask[i] ^= mex[i];
				C_container.m_mask[i] ^= mex[i];
			}
		}			
	}		
	QString MeDiff0_39, MeDiff40_79;
	string st;
	for(int round=0; round<=39; ++round)
	{
		
		st=boost::lexical_cast<std::string>(round);
		MeDiff0_39+=st.data();
		MeDiff0_39+=":{";
		//for(int bt=31; bt>=0; --bt)
		for(int bt=0; bt<32; ++bt)
		{
			if(bit32(F_container.m_mask[round],bt)==1)
			{
				st=boost::lexical_cast<std::string>(bt);
				MeDiff0_39+=st.data();
				MeDiff0_39+=",";
			}
		}
		MeDiff0_39+="}\n";
	}
	for(int round=40; round<=79; ++round)
	{
		
		st=boost::lexical_cast<std::string>(round);
		MeDiff40_79+=st.data();
		MeDiff40_79+=":{";
		//for(int bt=31; bt>=0; --bt)
		for(int bt=0; bt<32; ++bt)
		{
			if(bit32(F_container.m_mask[round],bt)==1)
			{
				st=boost::lexical_cast<std::string>(bt);
				MeDiff40_79+=st.data();
				MeDiff40_79+=",";
			}
		}
		MeDiff40_79+="}\n";
	}

	ui.PrintMdiff_0_39->setText(MeDiff0_39);
	ui.PrintMdiff_40_79->setText(MeDiff40_79);
}
void QtFace:: MdiffExpand_I()
{
	vector< vector<unsigned> > msgmask(16);

	QString i_qstring=ui.lineEdit->text();
	QString b_qstring=ui.lineEdit_2->text();
	unsigned msgoffset = i_qstring.toInt();
	unsigned diff_bit=b_qstring.toInt();
	msgmask[15].push_back(diff_bit%32);
	bool msglcext = true;
	uint32 me[16];
	for (unsigned k = 0; k < 16; ++k) {			
		me[k] = 0;
		for (unsigned j = 0; j < msgmask[k].size(); ++j)
			me[k] |= 1<<msgmask[k][j];
	}
	sha1_me_generalised(F_container.m_mask, me, msgoffset);
	sha1_me_generalised(B_container.m_mask, me, msgoffset);
	sha1_me_generalised(C_container.m_mask, me, msgoffset);

	QString MeDiff0_39, MeDiff40_79;
	string st;
	for(int round=0; round<=39; ++round)
	{
		
		st=boost::lexical_cast<std::string>(round);
		MeDiff0_39+=st.data();
		MeDiff0_39+=":{";
		//for(int bt=31; bt>=0; --bt)
		for(int bt=0; bt<32; ++bt)
		{
			if(bit32(F_container.m_mask[round],bt)==1)
			{
				st=boost::lexical_cast<std::string>(bt);
				MeDiff0_39+=st.data();
				MeDiff0_39+=",";
			}
		}
		MeDiff0_39+="}\n";
	}
	for(int round=40; round<=79; ++round)
	{
		
		st=boost::lexical_cast<std::string>(round);
		MeDiff40_79+=st.data();
		MeDiff40_79+=":{";
		//for(int bt=31; bt>=0; --bt)
		for(int bt=0; bt<32; ++bt)
		{
			if(bit32(F_container.m_mask[round],bt)==1)
			{
				st=boost::lexical_cast<std::string>(bt);
				MeDiff40_79+=st.data();
				MeDiff40_79+=",";
			}
		}
		MeDiff40_79+="}\n";
	}

	ui.PrintMdiff_0_39->setText(MeDiff0_39);
	ui.PrintMdiff_40_79->setText(MeDiff40_79);

}

void QtFace:: ConfigForward()
{
	bool * ok;
	QString Tmpstr;
	//mod m (1)
	Tmpstr=ui.modn_val_edit->text();
	F_container.modn=Tmpstr.toUInt();

	//mod i (0)
	Tmpstr=ui.modi_val_edit->text();
	F_container.modi=Tmpstr.toInt();

	//workdir (./data)
	Tmpstr=ui.workdir_val_edit->text();
	workdir=Tmpstr.toStdString();

	//inputfile ("")
	Tmpstr=ui.inputfile_val_edit->text();
	F_container.inputfile=Tmpstr.toStdString();
	
	
	//F_container.newinputpath (true)
	Tmpstr=ui.newinputpath_val_edit->text();
	if(Tmpstr.toInt()!=0)F_container.newinputpath=true;
	else
	{
		F_container.newinputpath=false;
	}

	//showinputpath (false)
	Tmpstr=ui.showinputpath_val_edit->text();
	if(Tmpstr.toInt()!=0)F_container.showinputpaths=true;
	else F_container.showinputpaths=false;


	//tstep (0)
	Tmpstr=ui.tstep_val_edit->text();
	F_container.t=Tmpstr.toInt( );

	//trange (2)
	Tmpstr=ui.trange_val_edit->text();
	F_container.trange=Tmpstr.toInt( );

	//maxconditions (2176)
	Tmpstr=ui.maxconditions_val_edit->text();
	F_container.maxcond=Tmpstr.toInt( );

	//condtbegin (-3)
	Tmpstr=ui.condtbegin_val_edit->text();
	F_container.tbegin=Tmpstr.toInt( );

	//includenaf (0)
	Tmpstr=ui.include_naf_val_edit->text();
	if(Tmpstr.toInt( )!=0)F_container.includenaf=true;
	else F_container.includenaf=false;

	//halfnafweight (0)
	Tmpstr=ui.halfnafweight_val_edit->text();
	if(Tmpstr.toInt( )!=0)F_container.halfnafweight=true;
	else F_container.halfnafweight=false;

	//maxweight (8)
	Tmpstr=ui.maxweight_val_edit->text();
	F_container.maxweight=Tmpstr.toInt( );

	//minweight (0)
	Tmpstr=ui.minweight_val_edit->text();
	F_container.minweight=Tmpstr.toInt( );

	//maxsdrs (2000)
	Tmpstr=ui.maxsdrs_val_edit->text();
	F_container.maxsdrs=Tmpstr.toInt( );

	//autobalance (3000000)
	Tmpstr=ui.autobalance_val_edit->text();
	F_container.ubound=Tmpstr.toInt( );

	//fillfraction (1)
	Tmpstr=ui.fillfraction_val_edit->text();
	F_container.fillfraction=Tmpstr.toDouble();

	//estimatefactor (4)
	Tmpstr=ui.estimate_val_edit->text();
	F_container.estimatefactor=Tmpstr.toDouble();

	//nafestimate (4)
	Tmpstr=ui.nafestimate_val_edit->text();
	F_container.nafestweight=Tmpstr.toInt( );

	//onemessagedif (0)
	Tmpstr=ui.onemessagediff_val_edit->text();
	if(Tmpstr.toInt( )!=0)F_container.onemessagediff=true;
	else F_container.onemessagediff=false;

	//expandprevmessagediff (0)
	Tmpstr=ui.expandprevmessagediff_val_edit->text();
	if(Tmpstr.toInt( )!=0)F_container.expandprevmessagediff=true;
	else F_container.expandprevmessagediff=false;

	//tunnelconditionsfile ("")
	Tmpstr=ui.tunnelconditionsfile_val_edit->text();
	tunnelsfilename=Tmpstr.toStdString();

	if(F_container.newinputpath==true)
	{
		ForwardInitPath.offset=- int(F_container.t) + 4;
		ForwardInitPath.path.resize(6);
		ForwardInitPath.me.resize(6);
		//ForwardInitPath.getme(F_container.t).mask=F_container.m_mask[F_container.t];
		//save(ForwardInitPath,"tmpForward.txt",hashutil::archive_type::text_archive);

		ofstream file1("ForwardInitPath.txt");
		show_path(ForwardInitPath,file1);
		file1.close();
		QFile *file=new QFile("ForwardInitPath.txt");
		file->open(QIODevice::ReadOnly|QIODevice::Text);
		Tmpstr= QString(file->readAll());
		ui.ForwardPathDisplay->setText(Tmpstr);
	}
	else
	{
		try
		{
			vector<sha1differentialpath> mytmp;
			load_bz2(mytmp, binary_archive, F_container.inputfile);
			ofstream file1("ForwardInitPath.txt");
			show_path(mytmp[0],file1);
			file1<<endl<<"Total: "<<mytmp.size()<<endl;
			file1.close();
			QFile *file=new QFile("ForwardInitPath.txt");
			file->open(QIODevice::ReadOnly|QIODevice::Text);
			Tmpstr= QString(file->readAll());
		}
		catch(std::exception& e)
		{
			Tmpstr=QString( e.what());
		}
		ui.ForwardPathDisplay->setText(Tmpstr);
	}

}

void QtFace::ConfigBackward()
{
	bool * ok;
	QString Tmpstr;
	//mod m (1)
	Tmpstr=ui.modn_val_edit_2->text();
	B_container.modn=Tmpstr.toUInt();

	//mod i (0)
	Tmpstr=ui.modi_val_edit_2->text();
	B_container.modi=Tmpstr.toInt();

	//workdir (./data)
	Tmpstr=ui.workdir_val_edit_2->text();
	workdir=Tmpstr.toStdString();

	//inputfile ("")
	Tmpstr=ui.inputfile_val_edit_2->text();
	B_container.inputfile=Tmpstr.toStdString();
	
	
	//F_container.newinputpath (true)
	Tmpstr=ui.newinputpath_val_edit_2->text();
	if(Tmpstr.toInt()!=0)B_container.newinputpath=true;
	else B_container.newinputpath=false;


	//showinputpath (false)
	Tmpstr=ui.showinputpath_val_edit_2->text();
	if(Tmpstr.toInt()!=0)B_container.showinputpaths=true;
	else B_container.showinputpaths=false;


	//tstep (0)
	Tmpstr=ui.tstep_val_edit_2->text();
	B_container.t=Tmpstr.toInt( );

	//trange (2)��
	Tmpstr=ui.trange_val_edit_2->text();
	B_container.trange=Tmpstr.toInt( );


	//maxconditions (2176)
	Tmpstr=ui.maxconditions_val_edit_2->text();
	B_container.maxcond=Tmpstr.toInt( );


	//condtend (80)
	Tmpstr=ui.condtend_val_edit->text();
	B_container.tend=Tmpstr.toInt( );

	//maxQ26upcond
	Tmpstr=ui.maxQ26upcond_val_edit->text();
	B_container.maxQ26upcond=Tmpstr.toUInt( );

	//includenaf (0)
	Tmpstr=ui.include_naf_val_edit_2->text();
	if(Tmpstr.toInt( )!=0)B_container.includenaf=true;
	else B_container.includenaf=false;

	//halfnafweight (0)
	Tmpstr=ui.halfnafweight_val_edit_2->text();
	if(Tmpstr.toInt( )!=0)B_container.halfnafweight=true;
	else B_container.halfnafweight=false;

	//maxweight (3)
	Tmpstr=ui.maxweight_val_edit_2->text();
	B_container.maxweight=Tmpstr.toInt( );

	//minweight (0)
	Tmpstr=ui.minweight_val_edit_2->text();
	B_container.minweight=Tmpstr.toInt( );

	//maxsdrs (1)
	Tmpstr=ui.maxsdrs_val_edit_2->text();
	B_container.maxsdrs=Tmpstr.toInt( );

	//autobalance (3)
	Tmpstr=ui.autobalance_val_edit_2->text();
	B_container.ubound=Tmpstr.toInt( );

	//fillfraction (1)
	Tmpstr=ui.fillfraction_val_edit_2->text();
	B_container.fillfraction=Tmpstr.toDouble();

	//estimatefactor (4)
	Tmpstr=ui.estimate_val_edit_2->text();
	B_container.estimatefactor=Tmpstr.toDouble();

	//nafestimate (8)
	Tmpstr=ui.nafestimate_val_edit_2->text();
	B_container.nafestweight=Tmpstr.toInt( );



	//onemessagedif (0)
	Tmpstr=ui.onemessagediff_val_edit_2->text();
	if(Tmpstr.toInt( )!=0)B_container.onemessagediff=true;
	else B_container.onemessagediff=false;

	//expandprevmessagediff (0)
	Tmpstr=ui.expandprevmessagediff_val_edit_2->text();
	if(Tmpstr.toInt( )!=0)B_container.expandprevmessagediff=true;
	else B_container.expandprevmessagediff=false;

	//tunnelconditionsfile ("")
	Tmpstr=ui.tunnelconditionsfile_val_edit_2->text();
	tunnelsfilename=Tmpstr.toStdString();

	if(B_container.newinputpath==true)
	{
		BackwardInitPath.offset=- int(B_container.t) + 4;
		BackwardInitPath.path.resize(6);
		BackwardInitPath.me.resize(6);

		ofstream file1("BackwardInitPath.txt");
		show_path(BackwardInitPath,file1);
		
		file1.close();
		QFile *file=new QFile("BackwardInitPath.txt");
		file->open(QIODevice::ReadOnly|QIODevice::Text);
		Tmpstr= QString(file->readAll());
		ui.BackwardPathDisplay->setText(Tmpstr);
	}
	else
	{
		try
		{
			vector<sha1differentialpath> mytmp;
			load_bz2(mytmp, binary_archive, B_container.inputfile);
			fstream file1("BackwardInitPath.txt");
			show_path(mytmp[0],file1);
			file1<<endl<<"Total: "<<mytmp.size()<<endl;
			file1.close();
			QFile *file=new QFile("BackwardInitPath.txt");
			file->open(QIODevice::ReadOnly|QIODevice::Text);
			Tmpstr= QString(file->readAll());
		}
		catch(std::exception& e)
		{
			Tmpstr=QString( e.what());
		}
		ui.BackwardPathDisplay->setText(Tmpstr);
	}


	
}

void QtFace::ConfigConnect()
{
	bool * ok;
	QString Tmpstr;
	//mod m (1)
	Tmpstr=ui.modn_val_edit_3->text();
	C_container.modn=Tmpstr.toUInt();

	//mod i (0)
	Tmpstr=ui.modi_val_edit_3->text();
	C_container.modi=Tmpstr.toInt();

	//workdir (./data)
	Tmpstr=ui.workdir_val_edit_3->text();
	workdir=Tmpstr.toStdString();

	//inputfilelow (data/paths2_0of1.bin)
	Tmpstr=ui.inputfilelow_val_edit->text();
	C_container.inputfilelow=Tmpstr.toStdString();

	//loworder (0)
	Tmpstr=ui.loworder_val_edit->text();
	C_container.loworder=Tmpstr.toInt();

	//inputfilehigh (data/paths8_0of1.bin)
	Tmpstr=ui.inputfilehigh_val_edit->text();
	C_container.inputfilehigh=Tmpstr.toStdString();

	//tstep (3)
	Tmpstr=ui.tstep_val_edit_3->text();
	C_container.t=Tmpstr.toInt();

	//showinputpaths (false)
	Tmpstr=ui.showinputpath_val_edit_3->text();
	if(Tmpstr.toInt()!=0)C_container.showinputpaths=true;
	else C_container.showinputpaths=false;

	//inputfileredo ("")
	Tmpstr=ui.inputfileredo_val_edit->text();
	C_container.inputfileredo=Tmpstr.toStdString();

	//Qcondstart (64)
	Tmpstr=ui.Qcondstart_val_edit->text();
	C_container.Qcondstart=Tmpstr.toInt();

	//maxcond (262144)
	Tmpstr=ui.maxcond_val_edit->text();
	C_container.bestpathcond=Tmpstr.toInt();

	//keepall
	Tmpstr=ui.keepall_val_edit->text();
	if(Tmpstr.toInt()!=0)C_container.keepall=true;
	else C_container.keepall=false;

	//determinelowestcond (false)
	Tmpstr=ui.determinelowestcond_val_edit->text();
	if(Tmpstr.toInt()!=0)C_container.determinelowestcond=true;
	else C_container.determinelowestcond=false;

	//splitmode (2)
	Tmpstr=ui.splitmode_val_edit->text();
	C_container.splitmode=Tmpstr.toUInt();


	//tunnelconditionsfile ("")
	Tmpstr=ui.tunnelconditionsfile_val_edit_3->text();
	tunnelsfilename=Tmpstr.toStdString();

	//threads (1)
	Tmpstr=ui.threads_val_edit->text();
	C_container.threads=Tmpstr.toInt();

	//showstats (false)
	Tmpstr=ui.showstats_val_edit->text();
	if(Tmpstr.toInt()!=0)C_container.showstats=true;
	else C_container.showstats=false;

	
	if (C_container.inputfilelow.size() == 0)
	{
		ui.ConnectDisplay->setText(QString("Error: inputfilelow must be given!"));
		
	}
	if (C_container.inputfilehigh.size() == 0)
	{
		ui.ConnectDisplay->setText(QString("Error: inputfilehigh must be given!"));
	}

	Connect_dostep(C_container);

	try
	{
		vector<sha1differentialpath> pathsout;
		//load_bz2(tmpbestpaths, workdir + "/bestpaths_c" + boost::lexical_cast<string>(bestpathcond), binary_archive);
		load_bz2(pathsout, workdir + "/bestpaths", binary_archive);
		ofstream file2("PrintBestPaths.txt");
		for(int i=0; i<1; ++i)
		{
			file2<<"Path "<<i+1<<"/"<<pathsout.size()<<endl;
			show_path(pathsout[i],file2);
			file2<<endl;
		}
		file2<<"Total: "<<pathsout.size()<<endl;
		file2.close();
		QFile *file=new QFile("PrintBestPaths.txt");
		file->open(QIODevice::ReadOnly|QIODevice::Text);
		Tmpstr= QString(file->readAll());
		ui.ConnectDisplay->setText(Tmpstr);


		file2.open("PrintBestPaths.txt");
		for(int i=0; i<pathsout.size(); ++i)
		{
			file2<<"Path "<<i+1<<"/"<<pathsout.size()<<endl;
			show_path(pathsout[i],file2);
			file2<<endl;
		}
		file2<<"Total: "<<pathsout.size()<<endl;
		file2.close();
	}
	catch (std::exception& e) {
		Tmpstr=QString(QString( "Runtime: "));
		string TimeConsumed=boost::lexical_cast<std::string>(runtime.time());
		Tmpstr+=QString(TimeConsumed.data());
		Tmpstr+=QString("\n No Connections Are Made!:");
		ui.ConnectDisplay->setText(Tmpstr);
	} catch (...) {
		Tmpstr=QString(QString( "Runtime: "));
		string TimeConsumed=boost::lexical_cast<std::string>(runtime.time());
		Tmpstr+=QString(TimeConsumed.data());

		Tmpstr+=QString("Unknown exception caught!!:");
	}

}
void QtFace::SetBitCondition()
{

	QString Tmpstr;
	int wordNum=(ui.WordNum_val_edit->text()).toInt();
	int bitNum=(ui.BitNum_val_edit->text()).toInt();
	Tmpstr=ui.ForBitCondition_val_edit->text();
	if(Tmpstr.startsWith("+")==true)
	{
		ForwardInitPath.setbitcondition(wordNum,bitNum,bc_plus);
	
	}
	else if(Tmpstr.startsWith("-")==true)
	{
		ForwardInitPath.setbitcondition(wordNum,bitNum,bc_minus);
	}
	else
	{
		ForwardInitPath.setbitcondition(wordNum,bitNum,hashutil::bc_constant);
	}
	ofstream file1("ForwardInitPath.txt");
	show_path(ForwardInitPath,file1);
	file1.close();
	
	QFile *file=new QFile("ForwardInitPath.txt");
	file->open(QIODevice::ReadOnly|QIODevice::Text);
	Tmpstr= QString(file->readAll());
	ui.ForwardPathDisplay->setText(Tmpstr);
}

void QtFace::SetBitConditionBack()
{
	QString Tmpstr;
	int wordNum=(ui.WordNum_val_edit_2->text()).toInt();
	int bitNum=(ui.BitNum_val_edit_2->text()).toInt();
	Tmpstr=ui.BackBitCondition_val_edit->text();
	if(Tmpstr.startsWith("+")==true)
	{
		BackwardInitPath.setbitcondition(wordNum,bitNum,bc_plus);
	
	}
	else if(Tmpstr.startsWith("-")==true)
	{
		BackwardInitPath.setbitcondition(wordNum,bitNum,bc_minus);
	}
	else
	{
		BackwardInitPath.setbitcondition(wordNum,bitNum,hashutil::bc_constant);
	}
	ofstream file1("BackwardInitPath.txt");
	show_path(BackwardInitPath,file1);
	file1.close();

	QFile *file=new QFile("BackwardInitPath.txt");
	file->open(QIODevice::ReadOnly|QIODevice::Text);
	Tmpstr= QString(file->readAll());
	ui.BackwardPathDisplay->setText(Tmpstr);
}

void QtFace::ExpandForward()
{

	QString Tmpstr;
	Forward_pathscache.clear();
	// Start job with given parameters
	F_container.set_parameters();
	for (unsigned tt = F_container.t; tt < F_container.t+F_container.trange; ++tt) {
		//cout << "delta_m[" << tt << "] = " << sdr(0,F_container.m_mask[tt]) << endl;
		Forward_path_container_autobalance containertmp = F_container;
		containertmp.t = tt;
		Forward_dostep(containertmp, true);

	//	ofstream file1("ForwardInitPath.txt");
	//	show_path(Forward_pathscache[0] ,file1);
	//	file1.close();
	//	QFile *file=new QFile("ForwardInitPath.txt");
	//	file->open(QIODevice::ReadOnly|QIODevice::Text);
	//	Tmpstr= QString(file->readAll());
	//	file->close();
	//	delete file;
	//	ui.ForwardPathDisplay->setText(Tmpstr);

	}
	F_container.t += F_container.trange;
	Forward_dostep(F_container);


	QString readfile=QString(workdir.data());
	readfile+= QString("/");
	readfile+= QString("paths"); 
	readfile+= QString(boost::lexical_cast<std::string>(F_container.t).data());
	readfile+=QString("_");
	readfile+= QString(boost::lexical_cast<std::string>(F_container.modi).data());
	readfile+=QString("of");
	readfile+=QString(boost::lexical_cast<std::string>(F_container.modn).data());

	load_bz2(Forward_pathscache,readfile.toStdString(),hashutil::archive_type::binary_archive);
		ofstream file1("ForwardFinalPath.txt");
		show_path(Forward_pathscache[0] ,file1);
		file1<<endl<<"Total: "<<Forward_pathscache.size()<<endl;
		file1.close();
		QFile *file=new QFile("ForwardFinalPath.txt");
		file->open(QIODevice::ReadOnly|QIODevice::Text);
		Tmpstr= QString(file->readAll());
		ui.ForwardPathDisplay->setText(Tmpstr);
		Forward_pathscache.clear();
}


void QtFace::ExpandBackward()
{
	QString Tmpstr;

	B_container.set_parameters();
	for (unsigned tt = B_container.t; tt > B_container.t-B_container.trange; --tt) {
		Backward_path_container_autobalance containertmp = B_container;
		containertmp.t = tt;
		Backward_dostep(containertmp, true);
	}
	B_container.t -= B_container.trange;
	Backward_dostep(B_container);

	QString readfile=QString(workdir.data());
	readfile+= QString("/");
	readfile+= QString("paths"); 
	readfile+= QString(boost::lexical_cast<std::string>(B_container.t).data());
	readfile+=QString("_");
	readfile+= QString(boost::lexical_cast<std::string>(B_container.modi).data());
	readfile+=QString("of");
	readfile+=QString(boost::lexical_cast<std::string>(B_container.modn).data());

	load_bz2(Backward_pathscache,readfile.toStdString(),hashutil::archive_type::binary_archive);
		ofstream file1("BackwardFinalPath.txt");
		show_path(Backward_pathscache[0] ,file1);
		file1<<endl<<"Total: "<<Backward_pathscache.size()<<endl;
		file1.close();
		QFile *file=new QFile("BackwardFinalPath.txt");
		file->open(QIODevice::ReadOnly|QIODevice::Text);
		Tmpstr= QString(file->readAll());
		ui.BackwardPathDisplay->setText(Tmpstr);
		Backward_pathscache.clear();

}


QtFace::~QtFace()
{

}
/*
void QtFace::MyShowTest()
{
	vector< vector<unsigned> > msgmask(16);

	QString i_qstring=ui.lineEdit->text();
	QString b_qstring=ui.lineEdit_2->text();
	unsigned msgoffset = i_qstring.toInt();
	unsigned diff_bit=b_qstring.toInt();
	msgmask[1].push_back((diff_bit+31)%32);
	msgmask[3].push_back((diff_bit+31)%32);
	msgmask[15].push_back(diff_bit%32);
	bool msglcext = true;
	uint32 me[16];
	for (unsigned k = 0; k < 16; ++k) {			
		me[k] = 0;
		for (unsigned j = 0; j < msgmask[k].size(); ++j)
			me[k] |= 1<<msgmask[k][j];
	}
	sha1_me_generalised(F_container.m_mask, me, msgoffset);
	sha1_me_generalised(B_container.m_mask, me, msgoffset);
	sha1_me_generalised(C_container.m_mask, me, msgoffset);
	if (msglcext) {
		uint32 met[16];
		uint32 mex[80];
		for (unsigned i = 0; i < 16; ++i)
			met[i] = rotate_left(me[i],5);

		sha1_me_generalised(mex, met, msgoffset+1);

		for (unsigned i = 0; i < 80; ++i)
		{
			F_container.m_mask[i] ^= mex[i];
			B_container.m_mask[i] ^= mex[i];
			C_container.m_mask[i] ^= mex[i];
		}

		sha1_me_generalised(mex, me, msgoffset+2);

		for (unsigned i = 0; i < 80; ++i)
		{
			F_container.m_mask[i] ^= mex[i];
			B_container.m_mask[i] ^= mex[i];
			C_container.m_mask[i] ^= mex[i];
		}

		for (unsigned i = 0; i < 16; ++i)
			met[i] = rotate_left(me[i],30);

		for (unsigned j = 3; j < 6; ++j) {
			sha1_me_generalised(mex, met, msgoffset+j);

			for (unsigned i = 0; i < 80; ++i)
			{
				F_container.m_mask[i] ^= mex[i];
				B_container.m_mask[i] ^= mex[i];
				C_container.m_mask[i] ^= mex[i];
			}
		}			
	}		
	QString MeDiff0_39, MeDiff40_79;
	string st;
	for(int round=0; round<=39; ++round)
	{
		
		st=boost::lexical_cast<std::string>(round);
		MeDiff0_39+=st.data();
		MeDiff0_39+=":{";
		//for(int bt=31; bt>=0; --bt)
		for(int bt=0; bt<32; ++bt)
		{
			if(bit32(F_container.m_mask[round],bt)==1)
			{
				st=boost::lexical_cast<std::string>(bt);
				MeDiff0_39+=st.data();
				MeDiff0_39+=",";
			}
		}
		MeDiff0_39+="}\n";
	}
	for(int round=40; round<=79; ++round)
	{
		
		st=boost::lexical_cast<std::string>(round);
		MeDiff40_79+=st.data();
		MeDiff40_79+=":{";
		//for(int bt=31; bt>=0; --bt)
		for(int bt=0; bt<32; ++bt)
		{
			if(bit32(F_container.m_mask[round],bt)==1)
			{
				st=boost::lexical_cast<std::string>(bt);
				MeDiff40_79+=st.data();
				MeDiff40_79+=",";
			}
		}
		MeDiff40_79+="}\n";
	}

	ui.label_3->setText(MeDiff0_39);
	ui.label_4->setText(MeDiff40_79);
}
*/