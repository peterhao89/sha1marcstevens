#include "qtface.h"
#include <QtWidgets/QApplication>
#include <stdexcept>
#include <iostream>
#include <fstream>
#include <vector>
#include <string>

#include <boost/lexical_cast.hpp>
#include <boost/program_options.hpp>

#include <hashutil5/rng.hpp>
#include <hashutil5/sdr.hpp>
#include <hashutil5/timer.hpp>
#include <hashutil5/booleanfunction.hpp>

#include "Forwad_main.hpp"
#include "Backward_main.hpp"
#include "Connect_main.hpp"

using namespace hashutil;
using namespace std;
namespace po = boost::program_options;
namespace fs = boost::filesystem;

boost::mutex mut;
std::string workdir;
uint64 pbcount = 0;
int lastlowpathindex = -1;
vector< vector<unsigned> > badcounts(85, vector<unsigned>(32,0));
hashutil::timer runtime(true);
Connect_path_container C_container;
Backward_path_container_autobalance B_container;
Forward_path_container_autobalance F_container;
std::string tunnelsfilename="";

vector<sha1differentialpath> Forward_pathscache;

vector<sha1differentialpath> Backward_pathscache;

sha1differentialpath ForwardInitPath;
sha1differentialpath BackwardInitPath;
int main(int argc, char *argv[])
{
	QApplication a(argc, argv);
	QtFace w;
	w.show();
	return a.exec();
}
